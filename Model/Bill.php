<?php

/**
 * Description of Bill
 *
 * @author Luka
 */

use Phalcon\Mvc\Model;

class Bill extends Model{
    protected $id;
    protected $date_from;
    protected $date_to;
    protected $priceLT;
    protected $priceHT;
    
    function getId() {
        return $this->id;
    }
    function getDate_from() {
        return $this->date_from;
    }

    function getDate_to() {
        return $this->date_to;
    }

    function getPriceLT() {
        return $this->priceLT;
    }

    function getPriceHT() {
        return $this->priceHT;
    }



}
