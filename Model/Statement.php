<?php

/**
 * Description of Statement
 *
 * @author Luka
 */

use Phalcon\Mvc\Model;

class Statement extends Model{
    protected $id;
    protected $date;
    protected $countLT;
    protected $countHT;
    //User $user;
    
    function getId() {
        return $this->id;
    }
    function getDate() {
        return $this->date;
    }

    function getCountLT() {
        return $this->countLT;
    }

    function getCountHT() {
        return $this->countHT;
    }
    
}
